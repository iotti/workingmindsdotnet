﻿function isNumberKey(evt, allow_dot) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        if (charCode != 44 || allow_dot==false ) {
            return false;
        }
    return true;
}
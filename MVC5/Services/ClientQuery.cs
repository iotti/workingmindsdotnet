﻿using MVC5.CustomFiletrs;
using MVC5.Models;
using System;
using System.Linq;


namespace MVC5.Services
{
    public class ClientQuery : BaseQuery
    {
        private IQueryable<Client> Search(ClientFilter value)
        {
            var clients = from c in db.Clients
                          select c;
            if (value != null)
            {
                if (!String.IsNullOrEmpty(value.name))
                    clients = clients.Where(c => c.name.ToLower().Contains(value.name.ToLower()));
                if (!String.IsNullOrEmpty(value.CPF))
                    clients = clients.Where(c => c.CPF.ToLower().Contains(value.CPF.ToLower()));
                if (!String.IsNullOrEmpty(value.email))
                    clients = clients.Where(c => c.email.ToLower().Contains(value.email.ToLower()));
            }
            return clients;
        }

        private IQueryable<Client> Sort(string sortOrder, IQueryable<Client> clients)
        {
            switch (sortOrder)
            {
                case "name_desc":
                    clients = clients.OrderByDescending(c => c.name);
                    break;
                case "email":
                    clients = clients.OrderBy(c => c.email);
                    break;
                case "email_desc":
                    clients = clients.OrderByDescending(c => c.email);
                    break;
                case "CPF":
                    clients = clients.OrderBy(c => c.CPF);
                    break;
                case "CPF_desc":
                    clients = clients.OrderByDescending(c => c.CPF);
                    break;

                default:
                    clients = clients.OrderBy(c => c.name);
                    break;
            }
            return clients;
        }

        // GET: Clients
        public IQueryable<Client> Results(string sortOrder, ClientFilter searchParams, int? page, dynamic ViewBag, int per_page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.SearchParams = searchParams;
            ViewBag.CurrentPage = page;
            ViewBag.per_page = per_page;
            IQueryable<Client> result = Sort(sortOrder, Search(searchParams)); 
            return result;
        }
    }
}
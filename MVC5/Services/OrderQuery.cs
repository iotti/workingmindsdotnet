﻿using System;
using System.Data.Entity;
using System.Linq;
using MVC5.Models;
using MVC5.CustomFiletrs;

namespace MVC5.Services
{
    public class OrderQuery : BaseQuery
    {
        private IQueryable<Order> Search(OrderFilter value)
        {
            var orders = from o in db.Orders.Include(o => o.client).Include(o => o.product)
                         select o;
            if (value != null)
            {
                if(value.client_id!=null)
                    orders = orders.Where(o => o.client_id == value.client_id);
                if (!String.IsNullOrEmpty(value.client_name))
                    orders = orders.Where(o => o.client.name.ToLower().Contains(value.client_name.ToLower()));
                if (!String.IsNullOrEmpty(value.product_name))
                    orders = orders.Where(o => o.product.name.ToLower().Contains(value.product_name.ToLower()));
                if (value.status > -1)
                    orders = orders.Where(o => (int)o.status == value.status);
                if (value.qtd > 0)
                    orders = orders.Where(o => o.qtd == value.qtd);

                if (value.min_total != null && value.max_total != null)
                    orders = orders.Where(o => (o.qtd * o.product.price) >= value.min_total && (o.qtd * o.product.price) <= value.max_total);
                else if (value.min_total != null && value.max_total == null)
                    orders = orders.Where(o => (o.qtd * o.product.price) >= value.min_total);
                else if (value.min_total == null && value.max_total != null)
                    orders = orders.Where(o => (o.qtd * o.product.price) <= value.max_total);

                if (value.date_from != null && value.date_to != null)
                    orders = orders.Where(o => o.OrderDate >= value.date_from && o.OrderDate <= value.date_to);
                else if (value.date_from != null && value.date_to == null)
                    orders = orders.Where(o => o.OrderDate >= value.date_from);
                else if (value.date_from == null && value.date_to != null)
                    orders = orders.Where(o => o.OrderDate <= value.date_to);

            }
            return orders;
        }

        private IQueryable<Order> Sort(string sortOrder, IQueryable<Order> orders)
        {            
            switch (sortOrder)
            {
                case "product_desc":
                    orders = orders.OrderByDescending(o => o.product.name);
                    break;
                case "client":
                    orders = orders.OrderBy(o => o.client.name);
                    break;
                case "client_desc":
                    orders = orders.OrderByDescending(o => o.client.name);
                    break;
                case "qtd":
                    orders = orders.OrderBy(o => o.qtd);
                    break;
                case "qtd_desc":
                    orders = orders.OrderByDescending(o => o.qtd);
                    break;
                case "total":
                    orders = orders.OrderBy(o => o.qtd * o.product.price);
                    break;
                case "total_desc":
                    orders = orders.OrderByDescending(o => o.qtd * o.product.price);
                    break;
                case "discount":
                    orders = orders.OrderBy(o => o.discount);
                    break;
                case "discount_desc":
                    orders = orders.OrderByDescending(o => o.discount);
                    break;
                case "status":
                    orders = orders.OrderBy(o => o.status);
                    break;
                case "status_desc":
                    orders = orders.OrderByDescending(o => o.status);
                    break;
                case "orderDate":
                    orders = orders.OrderBy(o => o.OrderDate);
                    break;
                case "orderDate_desc":
                    orders = orders.OrderByDescending(o => o.OrderDate);
                    break;

                default:
                    orders = orders.OrderBy(c => c.product.name);
                    break;
            }
            return orders;
        }

        // GET: Orders
        public IQueryable<Order> Results(string sortOrder, OrderFilter searchParams, int? page, dynamic ViewBag, int itemsPerPage = 20)
        {            
            ViewBag.CurrentSort = sortOrder;
            ViewBag.SearchParams = searchParams;
            ViewBag.CurrentPage = page;
            ViewBag.per_page = itemsPerPage;
            IQueryable<Order> result = Sort(sortOrder, Search(searchParams));           

            return result;
        }
    }
}
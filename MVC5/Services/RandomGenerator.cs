﻿using MVC5.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace MVC5.Services
{
    public class RandomGenerator
    {
        private SQLiteDatabase db = new SQLiteDatabase();
        private Random random = new Random((int)DateTime.Now.Ticks);

        private bool commit()
        {
            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private string RandomString(int Size)
        {
            string input = "abcdefghijklmnopqrstuvwxyz0123456789";
            return Randommize(Size, input);
        }

        private string RandomNumberString(int Size)
        {
            string input = "0123456789";
            return Randommize(Size, input);
        }

        private string Randommize(int Size, string input)
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < Size; i++)
            {
                ch = input[random.Next(input.Length)];
                builder.Append(ch);
            }
            return builder.ToString();
        }


        public void RandomProductBuilder()
        {
            var names = new[] { "Camisa", "Camiseta", "Calça", "Bermuda", "Blusa", "Jaqueta", "Pijama" };
            var sizes = new[] { "PP", "P", "M", "G", "GG", "XG" };
            var colors = new[] { "Preta", "Branca", "Azul", "Amarela", "Verde", "Vermelha" };

            foreach (var n in names)
            {
                foreach (var s in sizes)
                {
                    foreach (var c in colors)
                    {
                        Product p = new Product();
                        p.name = n + " " + s + " " + c;
                        p.barcode = RandomNumberString(20);
                        p.price = (float)(random.Next(10, 200) + (0.01f * random.Next(0, 99)));
                        db.Products.Add(p);
                        commit();
                    }
                }
            }
            
        }

        public void RandomOrdersBuilder(int count)
        {
            var clients = (from c in db.Clients
                           select c.Id).Count();
            var products = (from p in db.Products
                            select p.Id).Count();


            db.Database.ExecuteSqlCommand("delete from Orders where Id > 0");
            commit();

            for (int i = 0; i < count; ++i)
            {
                Order o = new Order();
                o.client_id = db.Clients.OrderBy(r => r.Id).Skip(random.Next(clients)).Take(1).First().Id;
                o.product_id = db.Products.OrderBy(r => r.Id).Skip(random.Next(products)).Take(1).First().Id;
                o.qtd = random.Next(1, 30);
                o.OrderDate = new DateTime(2016, 01, 01).AddDays(random.Next(740));
                o.status = (OrderSatus)random.Next(0, 3);
                db.Orders.Add(o);
                commit();
            }            
        }

        public void RandomNameBuilder(int count)
        {
            var first_names = new[] { "Amanda", "Alberto", "Bruna", "Bernardo", "Clara", "Carlos", "Daniela", "Douglas", "Eliana", "Elias", "Felicia", "Fernando", "Gabriela", "Geraldo", "Heloísa", "Hélio", "Ivana", "Ivo", "Julia", "João", "Larissa", "Lucas", "Maria", "Marcio" };
            var last_names = new[] { "Amaral", "Bastos", "Couto", "Davila", "Elano", "Ferreira", "Gonçalves", "Hermano", "Iotti", "Juqueira", "Lemos", "Menezes" };

            for (int i = 0; i < count; ++i)
            {
                Client c = new Client();
                c.name = first_names[random.Next(first_names.Length)] + " " + last_names[random.Next(last_names.Length)];
                c.email = RandomString(3) + "@" + RandomString(5) + ".com";
                c.CPF = RandomNumberString(11);
                db.Clients.Add(c);
                commit();

            }
        }
    }
}
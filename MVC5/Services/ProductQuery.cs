﻿using MVC5.CustomFiletrs;
using MVC5.Models;
using System;
using System.Linq;

namespace MVC5.Services
{
    public class ProductQuery : BaseQuery
    {

        protected IQueryable<Product> Search(ProductFilter value)
        {
            var products = from p in db.Products
                           select p;
            if (value != null)
            {
                if (!String.IsNullOrEmpty(value.name))
                    products = products.Where(p => p.name.ToLower().Contains(value.name.ToLower()));
                if (value.min_price != null && value.max_price != null)
                    products = products.Where(p => p.price >= value.min_price && p.price <= value.max_price);
                else if (value.min_price != null && value.max_price == null)
                    products = products.Where(p => p.price >= value.min_price);
                else if (value.min_price == null && value.max_price != null)
                    products = products.Where(p => p.price <= value.max_price);
                if (!String.IsNullOrEmpty(value.barcode))
                    products = products.Where(p => p.barcode.ToLower().Contains(value.barcode.ToLower()));
            }
            return products;
        }

        protected IQueryable<Product> Sort(string sortOrder, IQueryable<Product> products)
        {

            switch (sortOrder)
            {
                case "name_desc":
                    products = products.OrderByDescending(p => p.name);
                    break;
                case "price":
                    products = products.OrderBy(p => p.price);
                    break;
                case "price_desc":
                    products = products.OrderByDescending(p => p.price);
                    break;
                case "barcode":
                    products = products.OrderBy(p => p.barcode);
                    break;
                case "barcode_desc":
                    products = products.OrderByDescending(p => p.barcode);
                    break;

                default:
                    products = products.OrderBy(p => p.name);
                    break;
            }
            return products;
        }

        // GET: Products
        public IQueryable Results(string sortOrder, ProductFilter searchParams, int? page, dynamic ViewBag, int per_page)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.CurrentSort = sortOrder;
            ViewBag.SearchParams = searchParams;
            ViewBag.CurrentPage = page;
            ViewBag.per_page = per_page;
            var result = Sort(sortOrder, Search(searchParams));
            

            return result;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC5.Models;
using System.Text;

namespace MVC5.Services
{
    public class UserQuery : BaseQuery
    {

        public string CreateUser(User user)
        {
            user.password = Convert.ToBase64String(Encoding.Unicode.GetBytes(user.password));
            db.Users.Add(user);
            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                //string Notice = ex.InnerException.ToString();
                return ex.InnerException.ToString();
            }

            return "";
        }

        public bool UserLogin(User user)
        {
            try
            {
                User l = db.Users.Where(u => u.email.ToLower().Equals(user.email.ToLower())).First();
                return (l.password.Equals(Convert.ToBase64String(Encoding.Unicode.GetBytes(user.password))));
            }
            catch(Exception ex)
            {
                ex.ToString();
            }
            return false;
        }
    }
}
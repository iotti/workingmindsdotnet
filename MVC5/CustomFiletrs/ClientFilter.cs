﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MVC5.CustomFiletrs
{
    public class ClientFilter : BaseFilter
    {
        [Display(Name = "Nome")]
        public string name { get; set; }
        
        public string CPF { get; set; }


        [Display(Name = "E-mail")]
        public string email { get; set; }
    }
}
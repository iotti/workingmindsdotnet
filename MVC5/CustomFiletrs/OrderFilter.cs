﻿using MVC5.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVC5.CustomFiletrs
{
    public class OrderFilter : BaseFilter
    {
        public OrderFilter()
        {
            qtd = null;
            date_to = null;
            date_from = null;
            status = -1;
            client_id = null;

        }
        public int? client_id { get; set; }

        [Display(Name = "Produto")]
        public string product_name { get; set; }

        [Display(Name = "Cliente")]
        public string client_name { get; set; }

        [Display(Name = "Quantidade")]
        public int? qtd { get; set; }

        [Display(Name = "Pedido de")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? date_from { get; set; }

        [Display(Name = "Pedido até")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? date_to { get; set; }


        [Display(Name = "Status")]
        public int status { get; set; }


        [Display(Name = "Total De")]
        public float? min_total { get; set; }
        [Display(Name = "Total Até")]
        public float? max_total { get; set; }

    }
}
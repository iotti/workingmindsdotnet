﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVC5.CustomFiletrs
{
    public class ProductFilter : BaseFilter
    {
        [Display(Name = "Produto")]
        public string name { get; set; }

        [DataType(DataType.Currency)]
        [Display(Name = "Menor Preço")]
        public float? min_price { get; set; }

        [DataType(DataType.Currency)]
        [Display(Name = "Maior Preço")]
        public float? max_price { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Código de barras")]
        public string barcode { get; set; }
    }
}
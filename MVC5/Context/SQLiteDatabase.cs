﻿using System.Data.SQLite;
using System.Data.SQLite.EF6;
using System.Data.SQLite.Generic;
using System.Data.Common;
using System.Data;
using System;
using System.Data.Entity;
using MVC5.Models;

public class SQLiteDatabase : DbContext
{
    public SQLiteDatabase() { Database.SetInitializer<SQLiteDatabase>(null); }

    public DbSet<Client> Clients { get; set; }
    public DbSet<Product> Products { get; set; }
    public DbSet<Order> Orders { get; set; }
    public DbSet<User> Users { get; set; }
}

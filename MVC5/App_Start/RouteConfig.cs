﻿using System.Web.Mvc;
using System.Web.Routing;

namespace MVC5.App_Start
{
    public class RouteConfig
    {
        public static void Configure(RouteCollection routes)
        {
            routes.MapRoute(
                name: "Language",
                url: "{lang}/{controller}/{action}/{id}",
                defaults: new { controller = "Clients", action = "Index", id = UrlParameter.Optional },
                constraints: new { lang = @"pt|en" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Clients", action = "Index", id = UrlParameter.Optional }
            );
            
        }
    }
}
﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVC5.App_LocalResources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("MVC5.App_LocalResources.Resource", typeof(Resource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Todos.
        /// </summary>
        public static string All {
            get {
                return ResourceManager.GetString("All", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Voltar para lista.
        /// </summary>
        public static string BackToList {
            get {
                return ResourceManager.GetString("BackToList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Código de Barras.
        /// </summary>
        public static string Barcode {
            get {
                return ResourceManager.GetString("Barcode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nome do cliente.
        /// </summary>
        public static string ClientName {
            get {
                return ResourceManager.GetString("ClientName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cliente.
        /// </summary>
        public static string ClientOrder {
            get {
                return ResourceManager.GetString("ClientOrder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Clientes.
        /// </summary>
        public static string ClientTitle {
            get {
                return ResourceManager.GetString("ClientTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to De.
        /// </summary>
        public static string DateFrom {
            get {
                return ResourceManager.GetString("DateFrom", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Data do Pedido.
        /// </summary>
        public static string DateOrder {
            get {
                return ResourceManager.GetString("DateOrder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Até.
        /// </summary>
        public static string DateTo {
            get {
                return ResourceManager.GetString("DateTo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Apagar.
        /// </summary>
        public static string DeleteAction {
            get {
                return ResourceManager.GetString("DeleteAction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Remover Cliente.
        /// </summary>
        public static string DeleteClient {
            get {
                return ResourceManager.GetString("DeleteClient", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ao apagar este cliente, todos os seus pedidos também serão apagados! Deseja Continuar?.
        /// </summary>
        public static string DeleteClientMessage {
            get {
                return ResourceManager.GetString("DeleteClientMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Este pedido será apagado, Deseja continuar?.
        /// </summary>
        public static string DeleteOrder {
            get {
                return ResourceManager.GetString("DeleteOrder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Este produto será apagado e todos pedidos deste produto serão elminiados. Deseja continuar?.
        /// </summary>
        public static string DeleteProduct {
            get {
                return ResourceManager.GetString("DeleteProduct", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Detalhes.
        /// </summary>
        public static string DetailsAction {
            get {
                return ResourceManager.GetString("DetailsAction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Desconto.
        /// </summary>
        public static string Discount {
            get {
                return ResourceManager.GetString("Discount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Alterar.
        /// </summary>
        public static string EditAction {
            get {
                return ResourceManager.GetString("EditAction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Alterar Cliente.
        /// </summary>
        public static string EditClient {
            get {
                return ResourceManager.GetString("EditClient", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Alterar Pedido.
        /// </summary>
        public static string EditOrder {
            get {
                return ResourceManager.GetString("EditOrder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to E-mail.
        /// </summary>
        public static string Email {
            get {
                return ResourceManager.GetString("Email", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Filtrar.
        /// </summary>
        public static string FilterAction {
            get {
                return ResourceManager.GetString("FilterAction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CPF Inválido.
        /// </summary>
        public static string InvalidCPF {
            get {
                return ResourceManager.GetString("InvalidCPF", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to E-mail inválido.
        /// </summary>
        public static string InvalidEmail {
            get {
                return ResourceManager.GetString("InvalidEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Usuario não encontrado ou senha inválida..
        /// </summary>
        public static string InvalidLogin {
            get {
                return ResourceManager.GetString("InvalidLogin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Itens Por Página.
        /// </summary>
        public static string ItemPerPage {
            get {
                return ResourceManager.GetString("ItemPerPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Últimos Pedidos.
        /// </summary>
        public static string LastOrders {
            get {
                return ResourceManager.GetString("LastOrders", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Maior Preço.
        /// </summary>
        public static string MaxPrice {
            get {
                return ResourceManager.GetString("MaxPrice", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Menor Preço.
        /// </summary>
        public static string MinPrice {
            get {
                return ResourceManager.GetString("MinPrice", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Novo Cliente.
        /// </summary>
        public static string NewClient {
            get {
                return ResourceManager.GetString("NewClient", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Novo Pedido.
        /// </summary>
        public static string NewOrder {
            get {
                return ResourceManager.GetString("NewOrder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Novo Produto.
        /// </summary>
        public static string NewProduct {
            get {
                return ResourceManager.GetString("NewProduct", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Novo Usuário.
        /// </summary>
        public static string NewUser {
            get {
                return ResourceManager.GetString("NewUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pedido.
        /// </summary>
        public static string Order {
            get {
                return ResourceManager.GetString("Order", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ver Pedidos.
        /// </summary>
        public static string OrdersAction {
            get {
                return ResourceManager.GetString("OrdersAction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pedidos.
        /// </summary>
        public static string OrderTitle {
            get {
                return ResourceManager.GetString("OrderTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pag..
        /// </summary>
        public static string PageTitle {
            get {
                return ResourceManager.GetString("PageTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Senha.
        /// </summary>
        public static string Password {
            get {
                return ResourceManager.GetString("Password", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Preço.
        /// </summary>
        public static string Price {
            get {
                return ResourceManager.GetString("Price", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Produto.
        /// </summary>
        public static string Product {
            get {
                return ResourceManager.GetString("Product", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Produto.
        /// </summary>
        public static string ProductOrder {
            get {
                return ResourceManager.GetString("ProductOrder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Produtos.
        /// </summary>
        public static string ProductTitle {
            get {
                return ResourceManager.GetString("ProductTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Quantidade.
        /// </summary>
        public static string Qtd {
            get {
                return ResourceManager.GetString("Qtd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Criar Conta.
        /// </summary>
        public static string Register {
            get {
                return ResourceManager.GetString("Register", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Campo obrigatório.
        /// </summary>
        public static string RequiredField {
            get {
                return ResourceManager.GetString("RequiredField", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Gravar.
        /// </summary>
        public static string SaveAction {
            get {
                return ResourceManager.GetString("SaveAction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Situação.
        /// </summary>
        public static string Status {
            get {
                return ResourceManager.GetString("Status", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancelado.
        /// </summary>
        public static string StatusCancelled {
            get {
                return ResourceManager.GetString("StatusCancelled", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Em Aberto.
        /// </summary>
        public static string StatusOpened {
            get {
                return ResourceManager.GetString("StatusOpened", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pago.
        /// </summary>
        public static string StatusPaid {
            get {
                return ResourceManager.GetString("StatusPaid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Campo muito extenso.
        /// </summary>
        public static string TooLongField {
            get {
                return ResourceManager.GetString("TooLongField", resourceCulture);
            }
        }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using MVC5.Validators;
using System.Collections.Generic;
using System;
using MVC5.App_LocalResources;

namespace MVC5.Models
{
    public class User : BaseModel
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Email", ResourceType = typeof(Resource))]
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "RequiredField", ErrorMessageResourceType = typeof(Resource))]
        [EmailAddress(ErrorMessageResourceType = typeof(Resource), ErrorMessage = "InvalidEmail")]
        public string email { get; set; }

        [Display(Name = "Password", ResourceType = typeof(Resource))]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "RequiredField", ErrorMessageResourceType = typeof(Resource))]
        public string password { get; set; }

    }
}
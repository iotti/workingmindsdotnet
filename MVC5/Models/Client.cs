﻿using System.ComponentModel.DataAnnotations;
using MVC5.Validators;
using System.Collections.Generic;
using System;
using MVC5.App_LocalResources;
//using ResourceFilesSample;

namespace MVC5.Models
{
    /*
    CREATE TABLE Clients (
    id    INTEGER       PRIMARY KEY AUTOINCREMENT
                        NOT NULL,
    CPF   VARCHAR (11)  UNIQUE
                        NOT NULL,
    name  VARCHAR (128) NOT NULL,
    email VARCHAR (128) NOT NULL
                        UNIQUE
);
    */
    public class Client : BaseModel
    {
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        [Display(Name = "ClientName", ResourceType = typeof(Resource))]
        [Required(ErrorMessage = "RequiredField", ErrorMessageResourceType = typeof(Resource))]
        [StringLength(128, ErrorMessageResourceType = typeof(Resource), ErrorMessage = "TooLongField")]
        public string name { get; set; }

        [Required(ErrorMessage = "RequiredField", ErrorMessageResourceType = typeof(Resource))]
        [CustomValidationCPF(ErrorMessage = "InvalidCPF", ErrorMessageResourceType = typeof(Resource))]
        [StringLength(14, ErrorMessageResourceType = typeof(Resource), ErrorMessage = "TooLongField")]
        public string CPF { get; set; }       

        [Display(Name = "Email", ResourceType = typeof(Resource))]
        [Required(ErrorMessage = "RequiredField", ErrorMessageResourceType = typeof(Resource))]
        [EmailAddress(ErrorMessageResourceType = typeof(Resource), ErrorMessage = "InvalidEmail")]
        public string email { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}
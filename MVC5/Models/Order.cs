﻿using System.ComponentModel.DataAnnotations;
using MVC5.Validators;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.ComponentModel;
using MVC5.App_LocalResources;

namespace MVC5.Models
{
    /*
    CREATE TABLE Orders (
    Id         INTEGER  PRIMARY KEY AUTOINCREMENT
                        NOT NULL,
    product_id INTEGER  REFERENCES Products (Id) 
                        NOT NULL,
    client_id  INTEGER  REFERENCES Clients (Id) 
                        NOT NULL,
    qtd        INTEGER  NOT NULL,
    OrderDate  DATETIME NOT NULL
    );
    */
    public enum OrderSatus
    {
        [Display(Name = "StatusCancelled", ResourceType = typeof(Resource))]
        CANCELADO,
        [Display(Name = "StatusOpened", ResourceType = typeof(Resource))]
        EM_ABERTO,
        [Display(Name = "StatusPaid", ResourceType = typeof(Resource))]
        PAGO        
    }
    public class Order : BaseModel
    {

        public Order()
        {
            discount = 0;
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        [Display(Name = "ProductOrder", ResourceType = typeof(Resource))]
        public int product_id { get; set; }

        [Display(Name = "ClientOrder", ResourceType = typeof(Resource))]
        public int client_id { get; set; }

        [Display(Name = "Qtd", ResourceType = typeof(Resource))]
        public int qtd { get; set; }

        [Required(ErrorMessage = "RequiredField", ErrorMessageResourceType = typeof(Resource))]
        [Display(Name = "DateOrder", ResourceType = typeof(Resource))]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime OrderDate { get; set; }

        [Required(ErrorMessage = "RequiredField", ErrorMessageResourceType = typeof(Resource))]
        [Display(Name = "Status", ResourceType = typeof(Resource))]
        public OrderSatus status { get; set; }

        [Required(ErrorMessage = "RequiredField", ErrorMessageResourceType = typeof(Resource))]
        [Range(0, 100)]
        [Display(Name = "Discount", ResourceType = typeof(Resource))]
        public int discount { get; set; }


        [ForeignKey("product_id")]
        public virtual Product product { get; set; }

        [ForeignKey("client_id")]
        public virtual Client client { get; set; }

        public virtual float? total()
        {
            float? t = (this.product.price * this.qtd);
            t -= 0.01f*t*discount;
            return t;
        } 
    }
}
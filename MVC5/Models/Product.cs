﻿using MVC5.App_LocalResources;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVC5.Models
{
    /*
    CREATE TABLE Products (
    Id    INTEGER         PRIMARY KEY AUTOINCREMENT
                          NOT NULL,
    name  VARCHAR (128)   NOT NULL,
    price DECIMAL (10, 2) NOT NULL
    );
    */
    public class Product : BaseModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        [Display(Name = "Product", ResourceType = typeof(Resource))]
        [Required(ErrorMessage = "RequiredField", ErrorMessageResourceType = typeof(Resource))]
        [StringLength(128, ErrorMessageResourceType = typeof(Resource), ErrorMessage = "TooLongField")]
        public string name { get; set; }

        [DataType(DataType.Currency)]
        [Required(ErrorMessage = "RequiredField", ErrorMessageResourceType = typeof(Resource))]
        [Display(Name = "Price", ResourceType = typeof(Resource))]
        public float? price { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "RequiredField", ErrorMessageResourceType = typeof(Resource))]
        [Display(Name = "Barcode", ResourceType = typeof(Resource))]
        [StringLength(20, ErrorMessageResourceType = typeof(Resource), ErrorMessage = "TooLongField")]
        public string barcode { get; set; }

    }
}
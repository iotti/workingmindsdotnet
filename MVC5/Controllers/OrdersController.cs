﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using MVC5.Models;
using MVC5.Controllers;
using MVC5.CustomFiletrs;
using PagedList;
using MVC5.Infrastructure;
using MVC5.Services;

namespace MVC5
{
    public class OrdersController : ApplicationController
    {

        // GET: Orders
        [AuthorizeExceptApi]
        public ActionResult Index(string sortOrder, OrderFilter searchParams, int? page, int per_page = 20)
        {

            IQueryable<Order> result = new OrderQuery().Results(sortOrder,searchParams,page,ViewBag, per_page);
            
            int pageNumber = (page ?? 1);
            return AppView(result.ToPagedList(pageNumber, per_page));
        }

        // GET: Orders/Details/5
        [AuthorizeExceptApi]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return AppView(order);
        }

        // GET: Orders/Create
        [Authorize]
        public ActionResult Create()
        {
            ViewBag.client_id = new SelectList(db.Clients, "Id", "name");
            ViewBag.product_id = new SelectList(db.Products, "Id", "name");
            return View();
        }

        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "Id,product_id,client_id,qtd,OrderDate,status,discount")] Order order)
        {
            if (ModelState.IsValid)
            {
                db.Orders.Add(order);
                db.SaveChanges();
                return AppRedirect("Index", "Orders", order);
                //return RedirectToAction("Index");
            }

            ViewBag.client_id = new SelectList(db.Clients, "Id", "name", order.client_id);
            ViewBag.product_id = new SelectList(db.Products, "Id", "name", order.product_id);
            return AppView(order);
        }

        // GET: Orders/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return AppError(new HttpStatusCodeResult(HttpStatusCode.BadRequest));
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return AppError(HttpNotFound());
            }
            ViewBag.client_id = new SelectList(db.Clients, "Id", "name", order.client_id);
            ViewBag.product_id = new SelectList(db.Products, "Id", "name", order.product_id);
            return AppView(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "Id,product_id,client_id,qtd,OrderDate,status,discount")] Order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return AppRedirect("Index","Orders",order);
            }
            ViewBag.client_id = new SelectList(db.Clients, "Id", "CPF", order.client_id);
            ViewBag.product_id = new SelectList(db.Products, "Id", "name", order.product_id);
            return AppView(order);
        }

        // GET: Orders/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return AppError(new HttpStatusCodeResult(HttpStatusCode.BadRequest));
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return AppError(HttpNotFound());
            }
            return AppView(order);
        }

        // POST: Orders/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Order order = db.Orders.Find(id);
            db.Orders.Remove(order);
            db.SaveChanges();
            return AppRedirect("Index","Orders",order);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

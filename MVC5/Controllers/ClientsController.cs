﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using MVC5.Models;
using MVC5.Controllers;
using PagedList;
using MVC5.CustomFiletrs;
using MVC5.Services;
using MVC5.Infrastructure;

namespace MVC5
{
    
    public class ClientsController : ApplicationController
    {
        

        // GET: Clients
        [AuthorizeExceptApi]
        public ActionResult Index(string sortOrder, ClientFilter searchParams, int? page, int per_page = 20)
        {
            IQueryable<Client> result = new ClientQuery().Results(sortOrder,searchParams,page,ViewBag, per_page);             
            int pageNumber = (page ?? 1);
            return AppView(result.ToPagedList(pageNumber, per_page));
        }

        // GET: Clients/Details/5
        [AuthorizeExceptApi]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Client client = db.Clients.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            return AppView(client);
        }

        // GET: Clients/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Clients/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "CPF,name,email")] Client client)
        {
            if (ModelState.IsValid)
            {
                db.Clients.Add(client);
                try {
                    db.SaveChanges();
                }
                catch (Exception ex) {
                    string Notice = ex.InnerException.ToString();
                    return AppView(Notice);
                }
                
                return AppRedirect("Index", "Clients", client);
            }

            return AppView(client);
        }

        // GET: Clients/Edit/5
        [Authorize]
        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Client client = db.Clients.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            return AppView(client);
        }

        // POST: Clients/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "Id, CPF,email,name")] Client client)
        {
            if (ModelState.IsValid)
            {
                db.Entry(client).State = EntityState.Modified;
                db.SaveChanges();
                return AppRedirect("Index","Clients",client);
            }
            return AppView(client);
        }

        // GET: Clients/Delete/5
        [Authorize]
        public ActionResult Delete(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Client client = db.Clients.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            return AppView(client);
        }

        // POST: Clients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {
            Client client = db.Clients.Find(id);
            db.Clients.Remove(client);
            db.SaveChanges();
            return AppRedirect("Index", "Clients", client);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

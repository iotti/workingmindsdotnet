﻿using MVC5.CustomFiletrs;
using MVC5.Infrastructure;
using MVC5.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MVC5.Controllers
{
    public class ApplicationController : Controller
    {
        protected SQLiteDatabase db = new SQLiteDatabase();
        private string CurrentLanguageCode { get; set; }

        protected override void Initialize(RequestContext requestContext)
        {
            if (requestContext.RouteData.Values["lang"] != null && requestContext.RouteData.Values["lang"] as string != "null")
            {
                CurrentLanguageCode = (string)requestContext.RouteData.Values["lang"];
                if (CurrentLanguageCode != null)
                {
                    try
                    {
                        Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = new CultureInfo(CurrentLanguageCode);
                    }
                    catch (Exception)
                    {
                        throw new NotSupportedException($"Invalid language code '{CurrentLanguageCode}'.");
                    }
                }
            }
            base.Initialize(requestContext);
        }


        protected void Application_Error(Object sender, EventArgs e)
        {
            var raisedException = Server.GetLastError();

            // Process exception
            RenderError(raisedException);
        }

        public bool IsJsonRequest()
        {
            /*string requestedWith = Request.ServerVariables["HTTP_X_REQUESTED_WITH"] ?? string.Empty;
            return string.Compare(requestedWith, "XMLHttpRequest", true) == 0
                && Request.ContentType.ToLower().Contains("application/json");*/
            return Request.ContentType.ToLower().Contains("application/json");
        }


        public ActionResult RenderError(Exception ex)
        {
            return View("RenderError", ex);
        }

        public string GetQueryString(object obj)
        {
            var properties = from p in obj.GetType().GetProperties()
                             where p.GetValue(obj, null) != null
                             select p.Name + "=" + HttpUtility.UrlEncode(p.GetValue(obj, null).ToString());

            return String.Join("&", properties.ToArray());
        }

        /*[Obsolete("Não use JSON, use JsonSuccess")]
        protected JsonResult Json<T>(T data)
        {
            throw new InvalidOperationException("Não use JSON, use JsonSuccess");
        }*/

        protected ActionResult AppView(object o = null)
        {
            if (!IsJsonRequest())
                return View(o);
            return JsonSuccess(o);
        }

        protected ActionResult AppError(dynamic error)
        {
            if (!IsJsonRequest())
                return error;
            return JsonSuccess();
        }

        protected ActionResult AppRedirect(string actionName, string controllerName, object o)
        {
            if (!IsJsonRequest())
                return RedirectToAction(actionName, controllerName);
            return JsonSuccess(o);
        }

        protected ActionResult AppErrorView(object o)
        {
            if (!IsJsonRequest())
                return View(o);
            return JsonError(o);
        }

        protected StandardJsonResult JsonSuccess()
        {
            return new StandardJsonResult();
        }

        protected StandardJsonResult JsonSuccess(Object Data)
        {
            return new StandardJsonResult() { Data = Data };
        }

        protected StandardJsonResult JsonError()
        {
            return new StandardJsonResult();
        }

        protected StandardJsonResult JsonError(Object Data)
        {
            return new StandardJsonResult() { Data = Data };
        }
    }    

}
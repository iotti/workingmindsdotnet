﻿using MVC5.Models;
using MVC5.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MVC5.Controllers
{

   
    public class AccountController : ApplicationController
    {
        //
        // GET: /Account/

        public ActionResult Index()
        {
            return RedirectToAction("Login");
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {
                string Notice = new UserQuery().CreateUser(user);
                if (String.IsNullOrEmpty(Notice))
                    return AppRedirect("Login", "Account", user);
                else
                {
                    return AppView(Notice);
                }                    
            }

            return AppView(user);
        }

        public ActionResult Login()
        {
            return View("Login");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login([Bind(Include = "password,email")]User login)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Error = "Form is not valid; please review and try again.";
                return View("Login");
            }

            if (new UserQuery().UserLogin(login))
            {
                FormsAuthentication.RedirectFromLoginPage(login.email, true);
                return Redirect(String.IsNullOrEmpty(Request["ReturnUrl"]) ? "/" : Request["ReturnUrl"]);
            }

            ViewBag.Error = App_LocalResources.Resource.InvalidLogin;
            return View("Login");
        }

        public ActionResult Logout()
        {
            Session.Clear();
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }
    }
}
﻿using System.Web.Mvc;
using MVC5.Services;

namespace MVC5.Controllers
{
    public class HomeController : ApplicationController
    {
        
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        

        public ActionResult RandomProductBuilder()
        {
            new RandomGenerator().RandomProductBuilder();
            return RedirectToAction("Index", "Products");
        }

        public ActionResult RandomOrdersBuilder(int count)
        {
            new RandomGenerator().RandomOrdersBuilder(count);
            return RedirectToAction("Index", "Orders");
        }

        public ActionResult RandomNameBuilder(int count)
        {
            new RandomGenerator().RandomNameBuilder(count);
            return RedirectToAction("Index", "Clients");
        }       
    }
}
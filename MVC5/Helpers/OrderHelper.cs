﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC5.Helpers
{
    public class OrderHelper
    {
        public static string OrderLink(string sortOrder, string column_name)
        {
            return (sortOrder == column_name ? column_name + "_desc" : column_name);
        }
    }
}
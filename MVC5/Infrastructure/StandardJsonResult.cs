﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MVC5.Infrastructure
{
    public class StandardJsonResult : JsonResult
    {
        const string RequestRefused = "To allow GET requests, set JsonRequestBehavior to AllowGet.";

        public HttpStatusCode Status { get; set; }


        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            if (JsonRequestBehavior == JsonRequestBehavior.DenyGet &&
                String.Equals(context.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase))
                throw new InvalidOperationException(RequestRefused);

            HttpResponseBase response = context.HttpContext.Response;

            response.ContentType = string.IsNullOrEmpty(ContentType) ? "application/json" : ContentType;

            if (ContentEncoding != null)
            {
                response.ContentEncoding = ContentEncoding;
            }

            

            SerializeData(response, context.HttpContext.AllErrors);

        }

        protected virtual void SerializeData(HttpResponseBase response, Exception[] errorMsgs)
        {
            var originalData = Data;
            bool noError = errorMsgs == null || errorMsgs.Length == 0;

            Data = new
            {
                Success = noError,
                ItemCount = originalData.GetType().IsArray ? ((Array)originalData).Length : 1,
                Content = originalData,
                ErrorMessages = errorMsgs,
                RequestTime = DateTime.Now,
            };

            response.StatusCode = noError ? (int)HttpStatusCode.OK : (int)HttpStatusCode.BadRequest;


            response.Write(JsonConvert.SerializeObject(Data, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                PreserveReferencesHandling = PreserveReferencesHandling.Objects
            }));
        }
    }
}
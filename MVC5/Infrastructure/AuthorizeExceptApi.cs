﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC5.Infrastructure
{
    public class AuthorizeExceptApi : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if(!filterContext.HttpContext.Request.ContentType.ToLower().Contains("application/json"))
                base.OnAuthorization(filterContext);            
        }
    }
}